package orgstupidfatcat.bitbucket.myfunctionlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Cong on 2/20/2015.
 */
public class MenuFragment0 extends Fragment implements OnMapReadyCallback, View.OnClickListener {
    private View rootView;
    private final LatLng LOCATION_CENTRE_VIE = new LatLng(48.358889, -4.569931);
    private final LatLng LOCATION_RAK= new LatLng(48.358117, -4.569030);
    private final LatLng LOCATION_TB = new LatLng(48.359234, -4.570132);
    private GoogleMap map = null;
    private SupportMapFragment mMapFragment;

    private Button btnCV, btnTB, btnRAK;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.menu_layout0, container, false);

        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mMapFragment != null) {
            mMapFragment.getMapAsync(this);
        } else {
            Log.i("main", "mMapFragment is null");
        }
        btnCV = (Button) rootView.findViewById(R.id.btnCV);
        btnRAK = (Button) rootView.findViewById(R.id.btnRAK);
        btnTB = (Button) rootView.findViewById(R.id.btnTB);
        btnCV.setOnClickListener(this);
        btnRAK.setOnClickListener(this);
        btnTB.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        CameraUpdate update;
        if (map != null)
            switch (v.getId()) {
                case R.id.btnCV:
                    Log.i("main","btnCV");
                    update = CameraUpdateFactory.newLatLngZoom(LOCATION_CENTRE_VIE, 18);
                    map.animateCamera(update);
                    break;
                case R.id.btnTB:
                    Log.i("main","btnTB");
                    update = CameraUpdateFactory.newLatLngZoom(LOCATION_TB, 18);
                    map.animateCamera(update);
                    break;
                case R.id.btnRAK:
                    Log.i("main","btnRAK");
                    update = CameraUpdateFactory.newLatLngZoom(LOCATION_RAK, 18);
                    map.animateCamera(update);
                    break;

            }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.map = map;
        map.addMarker(new MarkerOptions()
                .position(LOCATION_CENTRE_VIE)
                .title("Centre Vie")
                .snippet("It's an amazing playground!"));
        map.addMarker(new MarkerOptions()
                .position(LOCATION_RAK)
                .title("Rak")
                .snippet("I'm hungry..."));
        map.addMarker(new MarkerOptions()
                .position(LOCATION_TB)
                .title("Telecom Bretagne")
                .snippet("A real discotheque."));
        map.setMyLocationEnabled(true);

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                View v = getActivity().getLayoutInflater().inflate(R.layout.info_window, null);
                TextView tvTitle = (TextView) v.findViewById(R.id.info_win_title);
                TextView tvCont = (TextView) v.findViewById(R.id.info_win_content);
                ImageView ivImage = (ImageView) v.findViewById(R.id.info_win_image);


                tvTitle.setText(marker.getTitle());
                tvCont.setText(marker.getSnippet());

                /**
                 *  it's just an example
                 *  need change to adapt from database
                 *  initial database_id <--> marker_id every time the user use this app.
                 *  marker_id --> datbase_id --> image
                  */

                if(marker.getTitle().equals("Centre Vie")) {
                    ivImage.setImageResource(R.drawable.cv);
                } else if (marker.getTitle().equals("Rak")) {
                    ivImage.setImageResource(R.drawable.rak);
                } else if (marker.getTitle().equals("Telecom Bretagne")) {
                    ivImage.setImageResource(R.drawable.tb);
                }
                return v;
            }
        });

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                FragmentManager fragmentManager = getChildFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();

                View v = getActivity().getLayoutInflater().inflate(R.layout.menu_layout1, null);
            }
        });
    }

//    public void onClick_CV(View v) {
//        if (map == null) {
//            Log.i("main", "map is null");
//        } else {
//            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_CENTRE_VIE, 16);
//            map.animateCamera(update);
//        }
//    }
//    public void onClick_RAK(View v) {
//        if (map == null)  Log.i("main","map is null");
//        else {
//            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_RAK, 16);
//            map.animateCamera(update);
//        }
//    }
//    public void onClick_TB(View v) {
//        if (map == null)  Log.i("main","map is null");
//        else {
//            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(LOCATION_TB, 16);
//            map.animateCamera(update);
//        }
//    }
}
