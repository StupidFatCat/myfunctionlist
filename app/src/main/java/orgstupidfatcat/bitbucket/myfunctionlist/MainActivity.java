package orgstupidfatcat.bitbucket.myfunctionlist;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks{

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private Fragment objFragment0, objFragment1, objFragment2;
    private int intPos = -1,lastPos = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));



    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        switch (position) {
            case 0:
                if (intPos != 0) {
                    if (objFragment0 == null){
                        objFragment0 = new MenuFragment0();
                        ft.add(R.id.container, objFragment0)
                                .hide(objFragment0);
                    }
//                    mMapFragment = SupportMapFragment.newInstance();
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.container, mMapFragment)
//                            .add(R.id.container, objFragment0)
//                            .commit();
//                    mMapFragment.getMapAsync(this);
                    hideFragment(lastPos, ft);
                            ft.show(objFragment0);



                    //get the control of map
                    //                mMapFragment.getMapAsync(this);
                        //                    SupportMapFragment supMapFragment =(SupportMapFragment)
                        //                    fragmentManager.findFragmentById(R.id.map);

                    intPos = 0;
                }
                break;
            case 1:
                if (intPos != 1) {
                    if (objFragment1 == null) {
                        objFragment1 = new MenuFragment1();
                        ft.add(R.id.container, objFragment1)
                                .hide(objFragment1);
                    }
                    hideFragment(lastPos, ft);
                    ft.show(objFragment1);
                    intPos = 1;
                }
                break;
            case 2:
                if (intPos != 2) {
                    if (objFragment2 == null) {
                        objFragment2 = new MenuFragment2();
                        ft.add(R.id.container, objFragment2)
                                .hide(objFragment2);
                    }
                    hideFragment(lastPos, ft);
                    ft.show(objFragment2);
                    intPos = 2;
                }
                break;
        }

        lastPos = intPos;
        ft.commit();
        // update the main content by replacing fragments

    }

    private void hideFragment(int lastPos, FragmentTransaction ft) {
        switch (lastPos) {
            case 0:
                ft.hide(objFragment0);
                break;
            case 1:
                ft.hide(objFragment1);
                break;
            case 2:
                ft.hide(objFragment2);
                break;
        }
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }


}
